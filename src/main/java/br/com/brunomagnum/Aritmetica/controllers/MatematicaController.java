package br.com.brunomagnum.Aritmetica.controllers;

import br.com.brunomagnum.Aritmetica.DTOs.EntradaDTO;
import br.com.brunomagnum.Aritmetica.DTOs.RespostaDTO;
import br.com.brunomagnum.Aritmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Quantidade de números insuficiente");
        }
        return matematicaService.soma(entradaDTO);
    }

    @PutMapping("/subtrai")
    public RespostaDTO subtrai(@RequestBody EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Quantidade de números insuficiente");
        }
        return matematicaService.subtrai(entradaDTO);
    }

    @PutMapping("/multiplica")
    public RespostaDTO multiplica(@RequestBody EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Quantidade de números insuficiente");
        }
        return matematicaService.multiplica(entradaDTO);
    }

    @PutMapping("/divide")
    public RespostaDTO divide(@RequestBody EntradaDTO entradaDTO){
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Quantidade de números insuficiente");
        } else if (entradaDTO.getNumeros().size() > 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "apenas 2 numeros são permitidos");
        }

        if(entradaDTO.getNumeros().get(0) < entradaDTO.getNumeros().get(1)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Dividendo é menor que o divisor");
        }
        return matematicaService.divide(entradaDTO);
    }
}