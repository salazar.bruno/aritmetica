package br.com.brunomagnum.Aritmetica.services;

import br.com.brunomagnum.Aritmetica.DTOs.EntradaDTO;
import br.com.brunomagnum.Aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero=0;
        for (int n: entradaDTO.getNumeros()){
            numero += n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;

    }

    public RespostaDTO subtrai(EntradaDTO entradaDTO) {
        int numero = entradaDTO.getNumeros().get(0);
        entradaDTO.getNumeros().remove(0);
        for (int n: entradaDTO.getNumeros()){
            numero -= n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;

    }

    public RespostaDTO multiplica(EntradaDTO entradaDTO) {
        int numero=1;
        for (int n: entradaDTO.getNumeros()){
            numero *= n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divide(EntradaDTO entradaDTO) {
        int numero = entradaDTO.getNumeros().get(0);
        entradaDTO.getNumeros().remove(0);
        for (int n: entradaDTO.getNumeros()){
            numero /= n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;

    }
}
